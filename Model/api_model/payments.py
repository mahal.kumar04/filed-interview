from typing import Optional
from datetime import datetime
from pydantic import BaseModel, validator


class CreditCardModel(BaseModel):
    credit_card_number: str
    card_holder: str
    expiration_date: datetime
    security_code: Optional[str]
    amount: int

    @validator("credit_card_number")
    def credit_card_validation(cls, v):
        digits = list(map(int, str(v)))
        odd_sum = sum(digits[-1::-2])
        even_sum = sum([sum(divmod(2 * d, 10)) for d in digits[-2::-2]])
        if (odd_sum + even_sum) % 10 != 0:
            raise ValueError("Invalid credit card number")
        return v

    @validator("expiration_date")
    def expiration_date_validation(cls, v):
        exp_date = datetime.strptime(v, "%a %b")
        current_date = datetime.now()
        if current_date > exp_date:
            raise ValueError("Credit card expired")
        return v

    @validator("amount")
    def amount_validation(cls, v):
        if v < 0:
            raise ("Amount cant be negetive")
        return v

    @validator("security_code")
    def security_number_validation(cls, v):
        num_str = str(v)
        if len(num_str) > 3:
            raise ValueError("Invalid security code")
        return v