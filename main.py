from flask import Flask , request , jsonify
from Service.payment import PaymentGatewayHandler
from flask_pydantic import validate
from pydantic import ValidationError
from Model.api_model.payments import CreditCardModel

app = Flask(__name__)


@app.route("/ProcessPayment", methods=["POST"])
@validate(body=CreditCardModel)
def process_payment():
    try:
        payment_details = request.json
        amount = payment_details.get("amount")
        if amount < 20:
            return jsonify(PaymentGatewayHandler(payment_details).cheap_payment_gateway()) 

        elif amount > 21 and amount < 500:
            return jsonify(PaymentGatewayHandler(payment_details).expensive_payment_gateway()) 

        else:
            return jsonify(PaymentGatewayHandler(payment_details).premium_payment_gateway()) 
    except ValidationError as e:
        return jsonify({"message":e}) ,400
    else:
        return jsonify({"message":" Internal server error"}), 500
    


if __name__ == "__main__":
    app.run(port=5000, debug=True)
