class PaymentGatewayHandler:

    def __init__(self,payment_details):
        self.payment_details = payment_details

    def cheap_payment_gateway(self):
        return {"data": f"""Thank you for making a purchase of {self.payment_details.get("amount")} from cheap payment gateway"""}, 200

    def expensive_payment_gateway(self):
        return {"data": f"""Thank you for making a purchase of {self.payment_details.get("amount")} from expensive payment gateway"""} ,200


    def premium_payment_gateway(self):
        return {"data": f"""Thank you for making a purchase of {self.payment_details.get("amount")} from premium payment gateway"""}, 200

